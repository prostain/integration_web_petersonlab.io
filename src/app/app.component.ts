import { Component } from '@angular/core';
import {
  animate,
  group,
  query,
  style,
  transition,
  trigger,
} from '@angular/animations';

@Component({
  selector: 'app-root',
  animations: [
    trigger('enterAnimation', [
      transition(':enter', [
        query('.header', style({ opacity: 0 })),
        query('.bg-white', style({ transform: 'translateX(-100%)' })),
        query('.bg-tomate', style({ transform: 'translateX(100%)' })),
        group([
          query('.header', animate('800ms ease-out', style({ opacity: 1 }))),
          query(
            '.bg-white',
            animate('500ms ease-out', style({ transform: 'translateX(0%)' }))
          ),
        ]),

        query(
          '.bg-tomate',
          animate('500ms ease-out', style({ transform: 'translateX(0%)' }))
        ),
        // style({ transform: 'translateX(-100%)' }),
        // animate('500ms ease-in', style({ transform: 'translateX(0)' }))
      ]),
      transition(':leave', [
        query(
          '.bg-tomate',
          animate('500ms ease-out', style({ transform: 'translateX(100%)' }))
        ),
        group([
          query('.header', animate('800ms ease-out', style({ opacity: 0 }))),
          query(
            '.bg-white',
            animate('500ms ease-out', style({ transform: 'translateX(-100%)' }))
          ),
        ]),
        //  animate('500ms ease-out', style({ transform: 'translateX(-100%)' }))
      ]),
    ]),
  ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Cabbage';

  menuOn = false;

  openMenu() {
    this.menuOn = !this.menuOn;
  }
}
