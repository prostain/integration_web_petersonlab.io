import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VegetablesComponent } from './vegetables/vegetables.component';
import { FruitsComponent } from './fruits/fruits.component';

const routes: Routes = [
  {
    redirectTo: '/fruits',
    pathMatch: 'full',
    path: '',
  },
  {
    component: FruitsComponent,
    path: 'fruits',
  },
  {
    component: VegetablesComponent,
    path: 'vegetables',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
